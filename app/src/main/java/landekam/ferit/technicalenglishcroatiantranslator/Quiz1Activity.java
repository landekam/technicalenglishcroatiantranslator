package landekam.ferit.technicalenglishcroatiantranslator;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.awt.font.TextAttribute;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Quiz1Activity extends AppCompatActivity {

    private List<Entry> entries = new ArrayList<>();
    private ImageView imageViewItem;
    private EditText editTextAnswer;
    private Button buttonConfirm;
    private int counter = 0;
    private int correctAnswers;
    private boolean firstQuiz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz1);
        Bundle data = getIntent().getExtras();
        entries = data.getParcelableArrayList("ENTRIES");
        correctAnswers = data.getInt("CORRECT_ANSWERS");
        firstQuiz = data.getBoolean("FIRST_QUIZ");
        InitializeUI();
    }

    private void InitializeUI() {
        imageViewItem = findViewById(R.id.imageViewItem);
        editTextAnswer = findViewById(R.id.editTextAnswer);
        buttonConfirm = findViewById(R.id.buttonConfirm);

        try {
            InputStream inputStream = getAssets().open("images/" + entries.get(counter).Position + ".jpg");
            Drawable drawable = Drawable.createFromStream(inputStream, null);
            imageViewItem.setImageDrawable(drawable);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        buttonConfirm.setOnClickListener(v -> {
            String answer = editTextAnswer.getText().toString().toLowerCase().trim();
            if(answer.equals(entries.get(counter).English)){
                correctAnswers++;
                Toast.makeText(Quiz1Activity.this, R.string.correct, Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(Quiz1Activity.this, R.string.incorrect, Toast.LENGTH_SHORT).show();
            }
            counter++;
            if (counter<5){
                editTextAnswer.setText("");
                try {
                    InputStream inputStream = getAssets().open("images/" + entries.get(counter).Position + ".jpg");
                    Drawable drawable = Drawable.createFromStream(inputStream, null);
                    imageViewItem.setImageDrawable(drawable);
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else {
                Collections.shuffle(entries);
                StartNextActivity();
            }
        });
    }

    private void StartNextActivity() {
        Intent intent;
        if (firstQuiz){
            int randomNumber = ThreadLocalRandom.current().nextInt(0, 3);
            if (randomNumber == 0){
                intent = new Intent(this, Quiz2Activity.class);
            }
            else if (randomNumber == 1){
                intent = new Intent(this, Quiz3Activity.class);
            }
            else {
                intent = new Intent(this, Quiz4Activity.class);
            }
            intent.putParcelableArrayListExtra("ENTRIES", (ArrayList<? extends Parcelable>) entries);
            intent.putExtra("FIRST_QUIZ", false);
        }
        else {
            intent = new Intent(this, ResultActivity.class);
        }
        intent.putExtra("CORRECT_ANSWERS", correctAnswers);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        ExitDialog();
        return;
    }

    private void ExitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.exit);
        builder.setMessage(R.string.exit_query);
        builder.setNegativeButton(R.string.no, (dialog, which) -> {

        });
        builder.setPositiveButton(R.string.yes, (dialog, which) -> finish());
        builder.show();
    }
}
