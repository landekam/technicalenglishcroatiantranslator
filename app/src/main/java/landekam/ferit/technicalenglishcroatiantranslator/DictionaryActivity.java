package landekam.ferit.technicalenglishcroatiantranslator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class DictionaryActivity extends AppCompatActivity {

    private List<String> englishDictionary = new ArrayList<>();
    private List<String> croatianDictionary = new ArrayList<>();
    boolean englishToCroatian = true;
    private Button buttonLanguage, buttonSearch;
    private EditText editTextSearch;
    private ListView listViewSearchResults;
    private SearchResultsAdapter searchResultsAdapter;
    private ArrayList<String> results = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);
        InitializeDictionaries();
        InitializeUI();
    }

    private void InitializeUI() {
        buttonLanguage = findViewById(R.id.buttonLanguage);
        buttonSearch = findViewById(R.id.buttonSearch);
        editTextSearch = findViewById(R.id.editTextSearch);
        listViewSearchResults = findViewById(R.id.listViewSearchResults);

        buttonLanguage.setOnClickListener(v -> {
            if(englishToCroatian){
                englishToCroatian = false;
                buttonLanguage.setText(R.string.croatian_to_english);
            }
            else {
                englishToCroatian = true;
                buttonLanguage.setText(R.string.english_to_croatian);
            }
        });

        buttonSearch.setOnClickListener(v -> {
            if (editTextSearch.getText().toString().equals("")){
                Toast.makeText(DictionaryActivity.this, R.string.field_empty, Toast.LENGTH_SHORT).show();
            }
            else {
                SearchWords();
            }
        });

        searchResultsAdapter = new SearchResultsAdapter(this, results);
        listViewSearchResults.setAdapter(searchResultsAdapter);
    }

    private void SearchWords() {
        String searchText = editTextSearch.getText().toString().toLowerCase().trim();
        results.clear();
        searchResultsAdapter.notifyDataSetChanged();
        String result;
        if(englishToCroatian){
            for (String entry: englishDictionary) {
                if(entry.startsWith(searchText)){
                    result = entry;
                    int position = englishDictionary.indexOf(entry);
                    result += " - " + croatianDictionary.get(position);
                    result = result.replace("_", " ");
                    results.add(result);
                }
            }
        }
        else {
            for (String entry: croatianDictionary) {
                if(entry.startsWith(searchText)){
                    result = entry;
                    int position = croatianDictionary.indexOf(entry);
                    result += " - " + englishDictionary.get(position);
                    result = result.replace("_", " ");
                    results.add(result);
                }
            }
        }
        if(results.isEmpty()){
            Toast.makeText(DictionaryActivity.this, R.string.no_results,Toast.LENGTH_SHORT).show();
        }
        searchResultsAdapter.notifyDataSetChanged();
    }

    private void InitializeDictionaries() {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new InputStreamReader(getAssets().open("automl/dict.txt")));
            String line;
            while((line = reader.readLine()) != null){
                englishDictionary.add(line);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            reader = new BufferedReader(new InputStreamReader(getAssets().open("croDict.txt")));
            String line;
            while((line = reader.readLine()) != null){
                croatianDictionary.add(line);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
