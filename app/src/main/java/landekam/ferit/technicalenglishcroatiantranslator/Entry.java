package landekam.ferit.technicalenglishcroatiantranslator;

import android.os.Parcel;
import android.os.Parcelable;


public class Entry implements Parcelable{
    public String English, Croatian;
    public int Position;

    public Entry(String english, String croatian, int position){
        English = english;
        Croatian = croatian;
        Position = position;
    }


    protected Entry(Parcel in) {
        English = in.readString();
        Croatian = in.readString();
        Position = in.readInt();
    }

    public static final Creator<Entry> CREATOR = new Creator<Entry>() {
        @Override
        public Entry createFromParcel(Parcel in) {
            return new Entry(in);
        }

        @Override
        public Entry[] newArray(int size) {
            return new Entry[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(English);
        dest.writeString(Croatian);
        dest.writeInt(Position);
    }
}
