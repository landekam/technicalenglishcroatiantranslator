package landekam.ferit.technicalenglishcroatiantranslator;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.SystemClock;
import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.common.modeldownload.FirebaseLocalModel;
import com.google.firebase.ml.common.modeldownload.FirebaseModelManager;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabel;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabeler;
import com.google.firebase.ml.vision.label.FirebaseVisionOnDeviceAutoMLImageLabelerOptions;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ImageClassifier {

    private static String TAG = "Translator";

    private static String LOCAL_MODEL_NAME = "automl_image_labeling_model";

    private static String LOCAL_MODEL_PATH = "automl/manifest.json";

    private static float CONFIDENCE_THRESHOLD = 0.6f;

    private FirebaseVisionImageLabeler labeler;

    private List<String> englishDictionary = new ArrayList<>();
    private List<String> croatianDictionary = new ArrayList<>();

    public ImageClassifier(Context context) throws FirebaseMLException {

        FirebaseModelManager.getInstance()
                .registerLocalModel(
                        new FirebaseLocalModel.Builder(LOCAL_MODEL_NAME)
                                .setAssetFilePath(LOCAL_MODEL_PATH)
                                .build()
                );

        FirebaseVisionOnDeviceAutoMLImageLabelerOptions options = new FirebaseVisionOnDeviceAutoMLImageLabelerOptions.Builder()
                .setConfidenceThreshold(CONFIDENCE_THRESHOLD)
                .setLocalModelName(LOCAL_MODEL_NAME)
                .build();

        labeler = FirebaseVision.getInstance().getOnDeviceAutoMLImageLabeler(options);

        BufferedReader reader;
        try {
            reader = new BufferedReader(new InputStreamReader(context.getAssets().open("automl/dict.txt")));
            String line;
            while((line = reader.readLine()) != null){
                englishDictionary.add(line);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            reader = new BufferedReader(new InputStreamReader(context.getAssets().open("croDict.txt")));
            String line;
            while((line = reader.readLine()) != null){
                croatianDictionary.add(line);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public Task<String> classifyFrame(Bitmap bitmap)  {
        if (labeler == null) {
            Log.e(TAG, "Image classifier has not been initialized; Skipped.");
            Exception e = new IllegalStateException("Uninitialized Classifier.");

            TaskCompletionSource completionSource = new TaskCompletionSource<String>();
            completionSource.setException(e);
            return completionSource.getTask();
        }

        long startTime = SystemClock.uptimeMillis();
        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);

        return labeler.processImage(image).continueWith(task -> {
            long endTime = SystemClock.uptimeMillis();
            Log.d(TAG, "Time to run model inference: " + (endTime - startTime));

            List<FirebaseVisionImageLabel> labelProbList = task.getResult();


            String textToShow = "";
            if (labelProbList.isEmpty()){
                textToShow += "No Result";
            }
            else{
                textToShow += printLabel(labelProbList);
            }
            return textToShow;
        });

    }

    private String printLabel(List<FirebaseVisionImageLabel> labels) {
        String topLabel = labels.get(0).getText();
        int position = englishDictionary.indexOf(topLabel);
        topLabel += " - " + croatianDictionary.get(position);
        topLabel = topLabel.replace("_", " ");
        topLabel += "\n" + String.format("%.2f", (labels.get(0).getConfidence() * 100))  + "%";
        return topLabel;
    }

    public void close() {
        try {
            labeler.close();
        } catch (IOException e) {
            Log.e(TAG, "Unable to close the labeler instance", e);
        }

    }

}
