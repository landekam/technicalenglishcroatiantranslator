package landekam.ferit.technicalenglishcroatiantranslator;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class StudyActivity extends AppCompatActivity {

    private ImageView imageViewItem;
    private TextView textViewTranslation;
    private Button buttonNext;

    private List<String> englishDictionary = new ArrayList<>();
    private List<String> croatianDictionary = new ArrayList<>();
    private List<Entry> entries = new ArrayList<>();
    private int counter = 0;
    private int set;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study);
        Bundle data = getIntent().getExtras();
        set = data.getInt("SET");
        InitializeDictionaries();
        InitializeUI();
    }

    private void InitializeUI() {
        imageViewItem = findViewById(R.id.imageViewItem);
        textViewTranslation = findViewById(R.id.textViewTranslation);
        buttonNext = findViewById(R.id.buttonNext);


        try {
            InputStream inputStream = getAssets().open("images/" + entries.get(counter).Position + ".jpg");
            Drawable drawable = Drawable.createFromStream(inputStream, null);
            imageViewItem.setImageDrawable(drawable);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        textViewTranslation.setText(entries.get(counter).English + " - " + entries.get(counter).Croatian);

        buttonNext.setOnClickListener(v -> {
           counter++;
           if(counter < 5){
               try {
                   InputStream inputStream = getAssets().open("images/" + entries.get(counter).Position + ".jpg");
                   Drawable drawable = Drawable.createFromStream(inputStream, null);
                   imageViewItem.setImageDrawable(drawable);
                   inputStream.close();
               } catch (IOException e) {
                   e.printStackTrace();
               }
               textViewTranslation.setText(entries.get(counter).English + " - " + entries.get(counter).Croatian);
           }
           else {
               Collections.shuffle(entries);
               StartQuizActivity();
           }
        });
    }

    private void StartQuizActivity() {
        int randomNumber = ThreadLocalRandom.current().nextInt(0, 5);
        Intent intent;
        if (randomNumber == 0){
            intent = new Intent(this, Quiz1Activity.class);
        }
        else if (randomNumber == 1){
            intent = new Intent(this, Quiz2Activity.class);
        }
        else if (randomNumber == 2){
            intent = new Intent(this, Quiz3Activity.class);
        }
        else {
            intent = new Intent(this, Quiz4Activity.class);
        }
        intent.putParcelableArrayListExtra("ENTRIES", (ArrayList<? extends Parcelable>) entries);
        intent.putExtra("CORRECT_ANSWERS", 0);
        intent.putExtra("FIRST_QUIZ", true);

        startActivity(intent);
        finish();
    }

    private void InitializeDictionaries() {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new InputStreamReader(getAssets().open("automl/dict.txt")));
            String line;
            while((line = reader.readLine()) != null){
                englishDictionary.add(line);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            reader = new BufferedReader(new InputStreamReader(getAssets().open("croDict.txt")));
            String line;
            while((line = reader.readLine()) != null){
                croatianDictionary.add(line);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        InitializeEntries();
    }
    private void InitializeEntries() {
        if(set == 0){
            ArrayList<Integer> randomNumbers = new ArrayList<>();
            int randomNumber;
            while (randomNumbers.size() < 5){
                randomNumber = ThreadLocalRandom.current().nextInt(0, englishDictionary.size());
                if(randomNumbers.indexOf(randomNumber) == -1){
                    randomNumbers.add(randomNumber);
                }
            }
            for (int number: randomNumbers) {
                entries.add(new Entry(englishDictionary.get(number).replace("_", " "), croatianDictionary.get(number), number));
            }
        }
        else {
            for(int i = (set - 1) * 5; i < (set - 1) * 5 + 5; i++){
                entries.add(new Entry(englishDictionary.get(i).replace("_", " "), croatianDictionary.get(i), i));
            }
        }
    }

    @Override
    public void onBackPressed() {
        ExitDialog();
        return;
    }

    private void ExitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.exit);
        builder.setMessage(R.string.exit_query);
        builder.setNegativeButton(R.string.no, (dialog, which) -> {

        });
        builder.setPositiveButton(R.string.yes, (dialog, which) -> finish());
        builder.show();
    }
}
