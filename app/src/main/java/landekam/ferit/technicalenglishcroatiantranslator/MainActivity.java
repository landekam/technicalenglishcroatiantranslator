package landekam.ferit.technicalenglishcroatiantranslator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private Button buttonObjectDetection, buttonDictionary, buttonChangeLanguage, buttonStudy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        InitializeUI();
    }

    private void setAppLocale(String localeCode){
        Resources resources = getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        Configuration config = resources.getConfiguration();
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN_MR1){
            config.setLocale(new Locale(localeCode.toLowerCase()));
        } else {
            config.locale = new Locale(localeCode.toLowerCase());
        }
        resources.updateConfiguration(config, dm);
        setContentView(R.layout.activity_main);
        InitializeUI();
    }

    private void InitializeUI() {
        buttonObjectDetection = findViewById(R.id.buttonObjectDetection);
        buttonDictionary = findViewById(R.id.buttonDictionary);
        buttonChangeLanguage = findViewById(R.id.buttonChangeLanguage);
        buttonStudy = findViewById(R.id.buttonStudy);


        buttonObjectDetection.setOnClickListener(v -> StartObjectDetectionActivity());

        buttonDictionary.setOnClickListener(v -> StartDictionaryActivity());

        buttonChangeLanguage.setOnClickListener(v -> {
            String locale = getResources().getString(R.string.locale);
            if(locale.equals("hr")){
                setAppLocale("en");
            }
            else {
                setAppLocale("hr");
            }
        });

        buttonStudy.setOnClickListener(v -> StartStudyActivity());
    }


    private void StartObjectDetectionActivity(){
        Intent intent = new Intent(this, ObjectDetectionActivity.class);
        startActivity(intent);
    }

    private void StartDictionaryActivity(){
        Intent intent = new Intent(this, DictionaryActivity.class);
        startActivity(intent);
    }

    private void StartStudyActivity() {
        Intent intent = new Intent(this, ChooseSetActivity.class);
        startActivity(intent);
    }
}
