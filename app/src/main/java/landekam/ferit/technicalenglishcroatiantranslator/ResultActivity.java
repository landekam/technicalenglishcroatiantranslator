package landekam.ferit.technicalenglishcroatiantranslator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    private int correctAnswers;
    private TextView textViewResult;
    private Button buttonClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Bundle data = getIntent().getExtras();
        correctAnswers = data.getInt("CORRECT_ANSWERS");
        InitializeUI();
    }

    private void InitializeUI() {
        textViewResult = findViewById(R.id.textViewResult);
        buttonClose = findViewById(R.id.buttonClose);

        textViewResult.setText(correctAnswers + "/10\n" + getString(R.string.correct_answers));

        buttonClose.setOnClickListener(v-> finish());
    }
}
