package landekam.ferit.technicalenglishcroatiantranslator;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Quiz4Activity extends AppCompatActivity {

    private List<Entry> entries = new ArrayList<>();
    private int counter = 0;
    private int correctAnswers;
    private int randomNumber;
    private TextView textViewItem;
    private ImageView imageViewAnswer1, imageViewAnswer2, imageViewAnswer3, imageViewAnswer4;
    private int image1Index, image2Index, image3Index, image4Index;
    private boolean firstQuiz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz4);
        Bundle data = getIntent().getExtras();
        entries = data.getParcelableArrayList("ENTRIES");
        correctAnswers = data.getInt("CORRECT_ANSWERS");
        firstQuiz = data.getBoolean("FIRST_QUIZ");
        InitializeUI();
    }

    private void InitializeUI() {
        textViewItem = findViewById(R.id.textViewItem);
        imageViewAnswer1 = findViewById(R.id.imageViewAnswer1);
        imageViewAnswer2 = findViewById(R.id.imageViewAnswer2);
        imageViewAnswer3 = findViewById(R.id.imageViewAnswer3);
        imageViewAnswer4 = findViewById(R.id.imageViewAnswer4);

        SetUpQuestion();

        imageViewAnswer1.setOnClickListener(v -> {
            if(image1Index == entries.get(counter).Position){
                correctAnswers++;
                Toast.makeText(Quiz4Activity.this, R.string.correct, Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(Quiz4Activity.this, R.string.incorrect, Toast.LENGTH_SHORT).show();
            }
            counter++;
            if (counter < 5){
                SetUpQuestion();
            }
            else {
                Collections.shuffle(entries);
                StartNextActivity();
            }
        });

        imageViewAnswer2.setOnClickListener(v -> {
            if(image2Index == entries.get(counter).Position){
                correctAnswers++;
                Toast.makeText(Quiz4Activity.this, R.string.correct, Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(Quiz4Activity.this, R.string.incorrect, Toast.LENGTH_SHORT).show();
            }
            counter++;
            if (counter < 5){
                SetUpQuestion();
            }
            else {
                Collections.shuffle(entries);
                StartNextActivity();
            }
        });

        imageViewAnswer3.setOnClickListener(v -> {
            if(image3Index == entries.get(counter).Position){
                correctAnswers++;
                Toast.makeText(Quiz4Activity.this, R.string.correct, Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(Quiz4Activity.this, R.string.incorrect, Toast.LENGTH_SHORT).show();
            }
            counter++;
            if (counter < 5){
                SetUpQuestion();
            }
            else {
                Collections.shuffle(entries);
                StartNextActivity();
            }
        });

        imageViewAnswer4.setOnClickListener(v -> {
            if(image4Index == entries.get(counter).Position){
                correctAnswers++;
                Toast.makeText(Quiz4Activity.this, R.string.correct, Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(Quiz4Activity.this, R.string.incorrect, Toast.LENGTH_SHORT).show();
            }
            counter++;
            if (counter < 5){
                SetUpQuestion();
            }
            else {
                Collections.shuffle(entries);
                StartNextActivity();
            }
        });
    }

    private void StartNextActivity() {
        Intent intent;
        if (firstQuiz){
            int randomNumber = ThreadLocalRandom.current().nextInt(0, 4);
            if (randomNumber == 0){
                intent = new Intent(this, Quiz1Activity.class);
            }
            else if (randomNumber == 1){
                intent = new Intent(this, Quiz2Activity.class);
            }
            else {
                intent = new Intent(this, Quiz3Activity.class);
            }
            intent.putParcelableArrayListExtra("ENTRIES", (ArrayList<? extends Parcelable>) entries);
            intent.putExtra("FIRST_QUIZ", false);
        }
        else {
            intent = new Intent(this, ResultActivity.class);
        }
        intent.putExtra("CORRECT_ANSWERS", correctAnswers);
        startActivity(intent);
        finish();
    }

    private void SetUpQuestion() {
        textViewItem.setText(entries.get(counter).English);
        do {
            randomNumber = ThreadLocalRandom.current().nextInt(0, 5);
        }while (randomNumber == counter);
        ArrayList<Integer> numbers = new ArrayList<>();
        for (int i = 0; i < 5; i++){
            if(i != randomNumber){
                numbers.add(i);
            }
        }
        Collections.shuffle(numbers);

        try {
            InputStream inputStream = getAssets().open("images/" + entries.get(numbers.get(0)).Position + ".jpg");
            Drawable drawable = Drawable.createFromStream(inputStream, null);
            imageViewAnswer1.setImageDrawable(drawable);
            image1Index = entries.get(numbers.get(0)).Position;
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            InputStream inputStream = getAssets().open("images/" + entries.get(numbers.get(1)).Position + ".jpg");
            Drawable drawable = Drawable.createFromStream(inputStream, null);
            imageViewAnswer2.setImageDrawable(drawable);
            image2Index = entries.get(numbers.get(1)).Position;
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            InputStream inputStream = getAssets().open("images/" + entries.get(numbers.get(2)).Position + ".jpg");
            Drawable drawable = Drawable.createFromStream(inputStream, null);
            imageViewAnswer3.setImageDrawable(drawable);
            image3Index = entries.get(numbers.get(2)).Position;
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            InputStream inputStream = getAssets().open("images/" + entries.get(numbers.get(3)).Position + ".jpg");
            Drawable drawable = Drawable.createFromStream(inputStream, null);
            imageViewAnswer4.setImageDrawable(drawable);
            image4Index = entries.get(numbers.get(3)).Position;
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        ExitDialog();
        return;
    }

    private void ExitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.exit);
        builder.setMessage(R.string.exit_query);
        builder.setNegativeButton(R.string.no, (dialog, which) -> {

        });
        builder.setPositiveButton(R.string.yes, (dialog, which) -> finish());
        builder.show();
    }
}
