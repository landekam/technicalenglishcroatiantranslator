package landekam.ferit.technicalenglishcroatiantranslator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.ml.common.FirebaseMLException;

public class ObjectDetectionActivity extends AppCompatActivity {

    private Button buttonTakePhoto, buttonLoadImage;
    private ImageView imageViewCaptured;
    private TextView textViewResult;
    private static final int PERMISSION_CODE = 1;
    private static final int IMAGE_CAPTURE_CODE = 2;
    private static final int PERMISSION_CODE2 = 3;
    private static final int IMAGE_LOAD_CODE = 4;
    Uri imageUri;
    private ImageClassifier classifier = null;
    private static final String TAG = "ObjectDetectionActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object_detection);
        InitializeUI();
    }

    private void InitializeUI() {
        buttonTakePhoto = findViewById(R.id.buttonTakePhoto);
        buttonLoadImage = findViewById(R.id.buttonLoadImage);
        imageViewCaptured = findViewById(R.id.imageViewCaptured);
        textViewResult = findViewById(R.id.textViewResult);

        buttonTakePhoto.setOnClickListener(v -> {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if(checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED
                        || checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){
                    String[] permission = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    requestPermissions(permission, PERMISSION_CODE);
                }
                else{
                    OpenCamera();
                }
            }
            else {
                OpenCamera();
            }
        });

        buttonLoadImage.setOnClickListener(v -> {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){
                    String[] permission = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    requestPermissions(permission, PERMISSION_CODE2);
                }
                else{
                    LoadImage();
                }
            }
            else {
                LoadImage();
            }
        });

        try {
            classifier = new ImageClassifier(this);
        } catch (FirebaseMLException e) {
            textViewResult.setText(R.string.failed_classifier_initialization);
        }
    }



    @Override
    protected void onDestroy() {
        classifier.close();
        super.onDestroy();
    }

    private void classifyImage(Bitmap bitmap) {
        if (classifier == null) {
            textViewResult.setText(R.string.uninitialized_classifier_or_invalid_context);
            return;
        }

        classifier.classifyFrame(bitmap).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                textViewResult.setText(task.getResult());
            } else {
                Exception e = task.getException();
                Log.e(TAG, "Error classifying frame", e);
                textViewResult.setText(e.getMessage());
            }
        });

    }

    private void OpenCamera() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.Images.Media.TITLE, "New Picture");
        imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE);
    }

    private void LoadImage() {
        Intent intent = new  Intent(Intent.ACTION_PICK);
        intent.setType("image/*");

        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);

        startActivityForResult(intent, IMAGE_LOAD_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == IMAGE_CAPTURE_CODE){
            if (resultCode == RESULT_OK) {
                imageViewCaptured.setImageURI(imageUri);
                classifyImage(((BitmapDrawable)imageViewCaptured.getDrawable()).getBitmap());
            }
        }
        if(requestCode == IMAGE_LOAD_CODE){
            if (resultCode == RESULT_OK) {
                imageUri = data.getData();
                imageViewCaptured.setImageURI(imageUri);
                classifyImage(((BitmapDrawable)imageViewCaptured.getDrawable()).getBitmap());
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PERMISSION_CODE:{
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    OpenCamera();
                    break;
                }
                else {
                    Toast.makeText(this,R.string.permission_denied, Toast.LENGTH_SHORT).show();
                    break;
                }
            }
            case PERMISSION_CODE2:{
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    LoadImage();
                    break;
                }
                else {
                    Toast.makeText(this,R.string.permission_denied, Toast.LENGTH_SHORT).show();
                    break;
                }
            }
        }
    }

}
