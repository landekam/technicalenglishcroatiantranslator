package landekam.ferit.technicalenglishcroatiantranslator;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class SearchResultsAdapter extends BaseAdapter {
    private Activity Context;
    private ArrayList<String> Results;
    private static LayoutInflater inflater = null;

    public SearchResultsAdapter(Activity context, ArrayList<String> results){
        Context = context;
        Results = results;
        inflater = (LayoutInflater) Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return Results.size();
    }

    @Override
    public Object getItem(int position) {
        return Results.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        itemView = (itemView==null) ? inflater.inflate(R.layout.search_results_layout,null):itemView;
        TextView textViewResult = itemView.findViewById(R.id.textViewResult);
        String selectedResult = Results.get(position);
        textViewResult.setText(selectedResult);
        return itemView;
    }
}
