package landekam.ferit.technicalenglishcroatiantranslator;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Quiz3Activity extends AppCompatActivity {

    private List<Entry> entries = new ArrayList<>();
    private int counter = 0;
    private int correctAnswers;
    private ImageView imageViewItem;
    private Button buttonAnswer1, buttonAnswer2, buttonAnswer3;
    ArrayList<Integer> randomNumbers = new ArrayList<>();
    private boolean firstQuiz;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz3);
        Bundle data = getIntent().getExtras();
        entries = data.getParcelableArrayList("ENTRIES");
        correctAnswers = data.getInt("CORRECT_ANSWERS");
        firstQuiz = data.getBoolean("FIRST_QUIZ");
        InitializeUI();
    }

    private void InitializeUI() {
        imageViewItem = findViewById(R.id.imageViewItem);
        buttonAnswer1 = findViewById(R.id.buttonAnswer1);
        buttonAnswer2 = findViewById(R.id.buttonAnswer2);
        buttonAnswer3 = findViewById(R.id.buttonAnswer3);

        SetUpQuestion();

        buttonAnswer1.setOnClickListener(v -> {
            if(buttonAnswer1.getText().toString().toLowerCase().equals(entries.get(counter).English)){
                correctAnswers++;
                Toast.makeText(Quiz3Activity.this, R.string.correct, Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(Quiz3Activity.this, R.string.incorrect, Toast.LENGTH_SHORT).show();
            }
            counter++;
            if (counter < 5){
                SetUpQuestion();
            }
            else {
                Collections.shuffle(entries);
                StartNextActivity();
            }
        });
        buttonAnswer2.setOnClickListener(v -> {
            if(buttonAnswer2.getText().toString().toLowerCase().equals(entries.get(counter).English)){
                correctAnswers++;
                Toast.makeText(Quiz3Activity.this, R.string.correct, Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(Quiz3Activity.this, R.string.incorrect, Toast.LENGTH_SHORT).show();
            }
            counter++;
            if (counter < 5){
                SetUpQuestion();
            }
            else {
                Collections.shuffle(entries);
                StartNextActivity();
            }
        });
        buttonAnswer3.setOnClickListener(v -> {
            if(buttonAnswer3.getText().toString().toLowerCase().equals(entries.get(counter).English)){
                correctAnswers++;
                Toast.makeText(Quiz3Activity.this, R.string.correct, Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(Quiz3Activity.this, R.string.incorrect, Toast.LENGTH_SHORT).show();
            }
            counter++;
            if (counter < 5){
                SetUpQuestion();
            }
            else {
                Collections.shuffle(entries);
                StartNextActivity();
            }
        });
    }

    private void StartNextActivity() {
        Intent intent;
        if (firstQuiz){
            int randomNumber = ThreadLocalRandom.current().nextInt(0, 4);
            if (randomNumber == 0){
                intent = new Intent(this, Quiz1Activity.class);
            }
            else if (randomNumber == 1){
                intent = new Intent(this, Quiz2Activity.class);
            }
            else {
                intent = new Intent(this, Quiz4Activity.class);
            }
            intent.putParcelableArrayListExtra("ENTRIES", (ArrayList<? extends Parcelable>) entries);
            intent.putExtra("FIRST_QUIZ", false);
        }
        else {
            intent = new Intent(this, ResultActivity.class);
        }
        intent.putExtra("CORRECT_ANSWERS", correctAnswers);
        startActivity(intent);
        finish();
    }

    private void SetUpQuestion() {
        try {
            InputStream inputStream = getAssets().open("images/" + entries.get(counter).Position + ".jpg");
            Drawable drawable = Drawable.createFromStream(inputStream, null);
            imageViewItem.setImageDrawable(drawable);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        randomNumbers.clear();
        randomNumbers.add(counter);
        int randomNumber;
        while(randomNumbers.size() < 3){
            randomNumber = ThreadLocalRandom.current().nextInt(0, 5);
            if(randomNumbers.indexOf(randomNumber) == -1){
                randomNumbers.add(randomNumber);
            }
        }
        Collections.shuffle(randomNumbers);
        buttonAnswer1.setText(entries.get(randomNumbers.get(0)).English);
        buttonAnswer2.setText(entries.get(randomNumbers.get(1)).English);
        buttonAnswer3.setText(entries.get(randomNumbers.get(2)).English);
    }

    @Override
    public void onBackPressed() {
        ExitDialog();
        return;
    }

    private void ExitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.exit);
        builder.setMessage(R.string.exit_query);
        builder.setNegativeButton(R.string.no, (dialog, which) -> {

        });
        builder.setPositiveButton(R.string.yes, (dialog, which) -> finish());
        builder.show();
    }
}
