package landekam.ferit.technicalenglishcroatiantranslator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class ChooseSetActivity extends AppCompatActivity {

    private Spinner spinnerChoice;
    private Button buttonConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_set);
        InitializeUI();
    }

    private void InitializeUI() {
        spinnerChoice = findViewById(R.id.spinnerChoice);
        buttonConfirm = findViewById(R.id.buttonConfirm);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.sets, R.layout.spinner_choice_item);
        adapter.setDropDownViewResource(R.layout.spinner_choice_dropdown_item);
        spinnerChoice.setAdapter(adapter);
        spinnerChoice.setSelection(0);

        buttonConfirm.setOnClickListener(v -> {
            StartStudyActivity();
        });
    }

    private void StartStudyActivity() {
        int set = spinnerChoice.getSelectedItemPosition();
        Intent intent = new Intent(this, StudyActivity.class);
        intent.putExtra("SET", set);
        startActivity(intent);
        finish();
    }
}
